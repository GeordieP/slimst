# SlimST

Took devastate mini and changed the colors.

Based on sublime theme [DevastateMini](https://github.com/shagabutdinov/sublime-devastate-mini) that was
based on [Devastate](https://github.com/vlakarados/devastate) that was based
on [Spacefunk](https://github.com/Twiebie/ST-Spacefunk).

Apply in Settings - User:
        "theme": "SlimST.sublime-theme",
